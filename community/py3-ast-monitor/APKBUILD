# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-ast-monitor
pkgver=0.3.3
pkgrel=0
pkgdesc="A wearable Raspberry Pi computer for cyclists"
url="https://github.com/firefly-cpp/AST-Monitor"
arch="noarch !s390x !riscv64 !armhf !ppc64le" # py3-sport-activities-features # py3-pyqt-feedback-flow
license="MIT"
depends="
	python3
	py3-geopy
	py3-matplotlib
	py3-pyqt-feedback-flow
	py3-qt5
	py3-qtwebengine
	py3-sport-activities-features
	py3-tcxreader
	"
checkdepends="py3-pytest"
makedepends="py3-gpep517 py3-poetry-core py3-installer"
subpackages="$pkgname-doc $pkgname-pyc"
source="https://github.com/firefly-cpp/AST-Monitor/archive/$pkgver/ast_monitor-$pkgver.tar.gz"
builddir="$srcdir/AST-Monitor-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
	install -Dm644 docs/preprints/2109-13334.pdf -t "$pkgdir"/usr/share/doc/$pkgname
}

sha512sums="
d423d3460cfb2548452e14707d9ae69a2610388c64c9d7b4a2decadbad2d4ae852bd6deae543e7d1074d3bbd69547c971122fc2695d88bfd74dc0ef53fc34802  ast_monitor-0.3.3.tar.gz
"
